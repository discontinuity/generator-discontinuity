"use strict"
var path = require("path")
var assert = require("yeoman-assert")
var helpers = require("yeoman-test")

describe("generator-discontinuity:react-component", () => {
  before(() => {
    return helpers
      .run(path.join(__dirname, "../generators/react-component"))
      .withPrompts({ someAnswer: true })
  })

  it("creates files", () => {
    assert.file(["dummyfile.txt"])
  })
})
