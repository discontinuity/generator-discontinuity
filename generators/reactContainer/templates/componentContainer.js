// @flow

import { connect } from 'react-redux'

import { action } from '../redux/actions'
import <%= name %> from './<%= name %>'

// ----------------------------------------------------------------------------

const mapStateToProps = (state, ownProps) => {
  return {
    property: true
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    functionName: () => {
      dispatch(action())
    }
  }
}

// ----------------------------------------------------------------------------

const <%= name %>Container = connect(
  mapStateToProps,
  mapDispatchToProps
)(<%= name %>)

export default <%= name %>Container
