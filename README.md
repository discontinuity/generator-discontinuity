# generator-discontinuity [![NPM version][npm-image]][npm-url]

The Yeoman generators used by Discontinuity S.r.l.

## Usage

### Command line

```
$ yo discontinuity:reactContainer
$ yo discontinuity:reactComponen
$ yo discontinuity:reactNativeComponent
```

### Atom



 [Learn more about Yeoman](http://yeoman.io/).

## development

To install the development version of the package

```
$ npm install --global .
$ yo --generators
```




[npm-image]: https://badge.fury.io/js/generator-discontinuity.svg
[npm-url]: https://npmjs.org/package/generator-discontinuity