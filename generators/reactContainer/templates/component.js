// @flow

import React from 'react'

// ----------------------------------------------------------------------------

class <%= name %> extends React.Component {
  render () {
    return (
      <div className={this.props.className}>
        content
      </div>
    )
  }
}

// ----------------------------------------------------------------------------

import styled from 'styled-components'
const Styled<%= name %> = styled(<%= name %>)`

`
export default Styled<%= name %>
