"use strict"
const Generator = require("yeoman-generator")
const changeCase = require("change-case")
const path = require("path")

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts)

    this.argument("name", {
      type: String,
      required: true,
      description: "Component name",
    })
  }

  writing() {
    const name = changeCase.pascalCase(this.options.name.replace(" ", "-"))
    this.fs.copyTpl(
      this.templatePath("index.js"),
      this.destinationPath(path.join(name, "index.js")),
      {
        name: name + "Container",
      }
    )

    this.fs.copyTpl(
      this.templatePath("component.js"),
      this.destinationPath(path.join(name, name + ".js")),
      {
        name: name,
      }
    )

    this.fs.copyTpl(
      this.templatePath("componentContainer.js"),
      this.destinationPath(path.join(name, name + "Container.js")),
      {
        name: name,
      }
    )
  }

  install() {
    this.installDependencies()
  }
}
